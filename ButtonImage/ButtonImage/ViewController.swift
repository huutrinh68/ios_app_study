//
//  ViewController.swift
//  ButtonImage
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!

    var count = 0

    let image0:UIImage = UIImage(named: "angelina-jolie-brad-pitt.jpg")!
    let image1:UIImage = UIImage(named: "lena.jpg")!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func buttonTapped(_ sender: Any) {
        count += 1

        if(count%2 == 0){
            button.setImage(image0, for: .normal)
        }else{
            button.setImage(image1, for: .normal)
        }
    }
}
