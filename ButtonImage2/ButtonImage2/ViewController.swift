//
//  ViewController.swift
//  ButtonImage2
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    let image0 = UIImage(named: "angelina-jolie-brad-pitt.jpg")
    let image1 = UIImage(named: "lena.jpg")
    
    var screenWidth:CGFloat = 0
    var screenHeight:CGFloat = 0
    
    let button = UIButton()
    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = view.frame.size.width
        screenHeight = view.frame.size.height
        
        button.frame = CGRect(x:0, y:screenHeight/2-screenWidth/2, width: screenWidth, height: screenHeight)
        
        button.setImage(image0, for: .normal)
        
        button.imageView?.contentMode = .scaleAspectFit
        button.contentHorizontalAlignment = .fill
        button.contentVerticalAlignment = .fill
        
        self.view.addSubview(button)
        
        button.addTarget(self, action: #selector(ViewController.buttonTapped(sender:)), for: .touchUpInside)
        
        self.view.backgroundColor = UIColor(displayP3Red: 0.937, green: 0.894, blue: 1.0, alpha: 1.0)
        
    }
    
    @objc func buttonTapped(sender: AnyObject){
        count += 1
        
        if(count%2 == 0){
            button.setImage(image0, for: .normal)
        }else{
            button.setImage(image1, for: .normal)
        }
    }
}

