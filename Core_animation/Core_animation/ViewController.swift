//
//  ViewController.swift
//  Core_animation
//
//  Created by huutrinh on 2019/07/02.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imgView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(red:0.9,green:1.0,blue:0.9,alpha:1.0)
        
        let img = UIImage(named: "angelina-jolie-brad-pitt@2x.jpg")!
        imgView = UIImageView(image: img)
        
        let iWidth = img.size.width
        let iHeight = img.size.height
        
        let screenWidth = self.view.bounds.size.width
        let screenHeight = self.view.bounds.size.height
        
        let scale = screenWidth/iWidth
        
        let rect = CGRect(x: CGFloat(0), y: CGFloat(0), width:
            iWidth*scale, height: iHeight*scale)
        imgView.frame = rect
        
        imgView.center = CGPoint(x: screenWidth/2, y: screenHeight/2)
        imgView.layer.cornerRadius = imgView.frame.size.width * 0.1
        imgView.clipsToBounds = true
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        rasterizationAnimation()
    }
    
    func rasterizationAnimation(){
        imgView.layer.shouldRasterize = true
        imgView.layer.rasterizationScale = 0.1
        imgView.layer.minificationFilter = CALayerContentsFilter.trilinear
        imgView.layer.magnificationFilter = CALayerContentsFilter.nearest
        
        let testAnimation = CABasicAnimation(keyPath: "rasterizationScale")
        
        testAnimation.fromValue = 0.1
        testAnimation.toValue = 1.0
        
        testAnimation.duration = 10.0
        
        testAnimation.isRemovedOnCompletion = false
        testAnimation.fillMode = CAMediaTimingFillMode.forwards
        
        imgView.layer.add(testAnimation, forKey: "testAnimation")
        self.view.addSubview(imgView)
    }
}

