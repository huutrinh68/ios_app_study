//
//  ViewController.swift
//  TableView
//
//  Created by huutrinh on 2019/07/02.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var table: UITableView!
    
    var selectedImage1: UIImage?
    var selectedImage2: UIImage?
    var selectedImage3: UIImage?
    
    let imgArray1: NSArray = ["angelina-jolie-brad-pitt@2x.jpg","angelina-jolie-brad-pitt@2x.jpg"]
    let imgArray2: NSArray = ["angelina-jolie-brad-pitt@2x.jpg","angelina-jolie-brad-pitt@2x.jpg", "angelina-jolie-brad-pitt@2x.jpg","angelina-jolie-brad-pitt@2x.jpg","angelina-jolie-brad-pitt@2x.jpg"]
    let imgArray3: NSArray = ["emmy-300x200.jpg"]
    
    let labelArray1: NSArray = ["8/23/ 16:04","8/23/ 16:15"]
    let labelArray2: NSArray = ["8/23/ 16:47","8/23/ 17:10",
                                "8/23/ 17:15","8/23/ 17:21","8/23/ 17:33"]
    let labelArray3: NSArray = ["8/23/ 17:41"]
    
    let sectionTitle: NSArray = ["section1", "section2", "section3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        return sectionTitle[section] as? String
    }
    
    func tableView(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return imgArray1.count
        }else if section == 1 {
            return imgArray2.count
        }else if section == 2 {
            return imgArray3.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "tableCell",
                                             for: indexPath)
        
        if indexPath.section == 0{
            print("indexPath.section == 0")
            let img = UIImage(named:imgArray1[indexPath.row] as! String)
            
            let imageView = cell.viewWithTag(1) as! UIImageView
            imageView.image = img
            
            let label = cell.viewWithTag(2) as! UILabel
            label.text = String(describing: labelArray1[indexPath.row])
        }
        else if indexPath.section == 1 {
            print("indexPath.section == 1")
            let img = UIImage(named:imgArray2[indexPath.row] as! String)
            let imageView = cell.viewWithTag(1) as! UIImageView
            imageView.image = img
            
            let label = cell.viewWithTag(2) as! UILabel
            label.text = String(describing: labelArray2[indexPath.row])
        }
        else if indexPath.section == 2 {
            print("indexPath.section == 2")
            let img = UIImage(named:imgArray3[indexPath.row] as! String)
            let imageView = cell.viewWithTag(1) as! UIImageView
            imageView.image = img
            
            let label = cell.viewWithTag(2) as! UILabel
            label.text = String(describing: labelArray3[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ table: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath: IndexPath){
        selectedImage1 = UIImage(named: imgArray1[indexPath.row] as! String)
        selectedImage2 = UIImage(named: imgArray2[indexPath.row] as! String)
        selectedImage3 = UIImage(named: imgArray3[indexPath.row] as! String)
        if selectedImage1 != nil {
            performSegue(withIdentifier: "toSubViewController", sender: nil)
        }
        
        if selectedImage2 != nil {
            performSegue(withIdentifier: "toSubViewController", sender: nil)
        }

        if selectedImage3 != nil {
            performSegue(withIdentifier: "toSubViewController", sender: nil)
        }

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if selectedImage1 != nil{
            if (segue.identifier == "toSubViewController") {
                let subVC: SubViewController = (segue.destination as? SubViewController)!
                subVC.selectedImg = selectedImage1
            }
        }
        if selectedImage2 != nil{
            if (segue.identifier == "toSubViewController") {
                let subVC: SubViewController = (segue.destination as? SubViewController)!
                subVC.selectedImg = selectedImage2
            }
        }
        if selectedImage3 != nil{
            if (segue.identifier == "toSubViewController") {
                let subVC: SubViewController = (segue.destination as? SubViewController)!
                subVC.selectedImg = selectedImage3
            }
        }
        
    }

}

