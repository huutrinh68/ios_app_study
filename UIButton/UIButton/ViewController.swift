//
//  ViewController.swift
//  UIButton
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var button_test: UIButton!
    @IBOutlet var label_test: UILabel!
    
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        label_test.text = "Swift Test"
        button_test.setTitle("Button", for: UIControl.State.normal)
    }

    @IBAction func button_tapped(_ sender: Any) {
        count += 1
        label_test.text = String(count)
    }
}

