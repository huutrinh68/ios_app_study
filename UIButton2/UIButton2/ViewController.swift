//
//  ViewController.swift
//  UIButton2
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let label = UILabel()
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.init(
            red: 0.71, green: 1.0, blue: 0.95, alpha: 1
        )
        
        let screenWidth:CGFloat = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        
        let button = UIButton()
        
        button.frame = CGRect(x:screenWidth/4, y:screenHeight/2, width: screenWidth/2, height: 50)
        
        button.setTitle("Tap me", for: UIControl.State.normal)
        
        button.backgroundColor = UIColor.init(
            red: 0.9, green: 0.9, blue: 0.9, alpha: 1
        )
        
        button.addTarget(self, action: #selector(ViewController.buttonTapped(sender:)), for: .touchUpInside)
        
        self.view.addSubview(button)
        
        label.frame = CGRect(x:screenWidth/4, y:200, width:screenWidth/2, height:50)
        
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 36)
        
        self.view.addSubview(label)
    }
    
    @objc func buttonTapped(sender: AnyObject){
        count += 1
        if(count%2 == 0){
            label.text = "Swift Test"
        }else{
            label.text = "tapped!"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

