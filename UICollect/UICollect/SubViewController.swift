//
//  SubViewController.swift
//  UICollect
//
//  Created by huutrinh on 2019/07/03.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import Foundation
import UIKit

class SubViewController: UIViewController{
    
    @IBOutlet weak var imageView: UIImageView!
    var selectedImg: UIImage!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        imageView.image = selectedImg
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
