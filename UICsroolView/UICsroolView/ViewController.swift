//
//  ViewController.swift
//  UICsroolView
//
//  Created by huutrinh on 2019/07/05.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var scrollScreenHeight:CGFloat!
    var scrollScreenWidth:CGFloat!
    
    let img:[String] = ["emmy-300x200.jpg","emmy-300x200.jpg","emmy-300x200.jpg"]
    
    var pageNum:Int!
    
    var imageWidth:CGFloat!
    var imageHeight:CGFloat!
    var screenSize:CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        screenSize = UIScreen.main.bounds
        
        scrollScreenWidth = screenSize.width
        
        let imageTop:UIImage = UIImage(named: img[0])!
        pageNum = img.count
        
        imageWidth = imageTop.size.width
        imageHeight = imageTop.size.height
        
        scrollScreenHeight = scrollScreenWidth*imageHeight/imageWidth
        
        for i in 0 ..< pageNum {
            let image:UIImage = UIImage(named: img[i])!
            let imageView = UIImageView(image: image)
            
            var rect:CGRect = imageView.frame
            rect.size.height = scrollScreenHeight
            rect.size.width = scrollScreenWidth
            
            imageView.frame = rect
            imageView.tag = i + 1
            
            self.scrollView.addSubview(imageView)
        }
        
        setupScrollImages()
        
    }
    
    func setupScrollImages(){
        
        let imageDummy:UIImage = UIImage(named:img[0])!
        var imgView = UIImageView(image:imageDummy)
        var subviews:Array = scrollView.subviews
        
        var px:CGFloat = 0.0
        let py:CGFloat = (screenSize.height - scrollScreenHeight)/2
        
        for i in 0 ..< subviews.count {
            imgView = subviews[i] as! UIImageView
            if (imgView.isKind(of: UIImageView.self) && imgView.tag > 0){
                
                var viewFrame:CGRect = imgView.frame
                viewFrame.origin = CGPoint(x: px, y: py)
                imgView.frame = viewFrame
                
                px += (scrollScreenWidth)
                
            }
        }
        
        let nWidth:CGFloat = scrollScreenWidth * CGFloat(pageNum)
        scrollView.contentSize = CGSize(width: nWidth, height: scrollScreenHeight)
        
    }

}

