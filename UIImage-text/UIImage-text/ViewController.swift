//
//  ViewController.swift
//  UIImage-text
//
//  Created by huutrinh on 2019/07/02.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(red: 0.99, green: 0.81, blue: 0.56, alpha: 1.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenWidth:CGFloat = view.frame.size.width
        let screenHeight:CGFloat = view.frame.size.height
        
        setup(sW: screenWidth, sH: screenHeight)
    }
    
    func setup(sW: CGFloat, sH: CGFloat){
        // create combine image
        let text = "Emmy's shopping in London"
        let font = UIFont.boldSystemFont(ofSize: 20)
        
        let imageEmmy: UIImage = UIImage(named: "emmy-300x200.jpg")!
        
        let imageWidth = imageEmmy.size.width
        let imageHeight = imageEmmy.size.height
        
        let rect = CGRect(x:0, y:0, width:imageWidth, height:imageHeight)
        
        let imageHangBag:UIImage = UIImage(named: "handbag.png")!
        
        let imageHandBagWidth = imageHangBag.size.width
        let imageHandBagHeight = imageHangBag.size.height
        
        UIGraphicsBeginImageContext(imageEmmy.size)
        
        imageEmmy.draw(in: rect)
        
        let textRect = CGRect(x:100, y:imageHeight/2+0, width:imageWidth-250, height:80)
        
        let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        
        let textFontAttributes = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: UIColor.yellow,
            NSAttributedString.Key.paragraphStyle : textStyle
        ]
        
        text.draw(in: textRect, withAttributes: textFontAttributes)
        
        let handBagRect = CGRect(x: imageWidth/2+10, y: imageHeight/2-20, width: imageHandBagWidth*2/2, height: imageHandBagHeight*2/2)
        imageHangBag.draw(in: handBagRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // show combine image
        let newImageView = UIImageView()
        newImageView.image = newImage
        
        let newImageWidth = newImage!.size.width
        let newImageHeight = newImage!.size.height
        
        let scale:CGFloat = sW/newImageWidth
        let newRect = CGRect(x: 0, y: 0, width: newImageWidth*scale, height: newImageHeight*scale)
        
        newImageView.frame = newRect
        
        let adjust:CGFloat = 40
        newImageView.center = CGPoint(x: sW/2, y: sH/2+adjust)
        
        self.view.addSubview(newImageView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

