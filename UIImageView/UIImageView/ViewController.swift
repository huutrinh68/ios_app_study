//
//  ViewController.swift
//  UIImageView
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initImageView()
    }
    
    private func initImageView(){
        let image1:UIImage = UIImage(named: "angelina-jolie-brad-pitt@2x.jpg")!
        
        let imageView = UIImageView(image:image1)
        
        let screenWidth:CGFloat = view.frame.size.width
        let screenHeight:CGFloat = view.frame.size.height
        
        let imgWidth:CGFloat = image1.size.width
        let imgHeight:CGFloat = image1.size.height
        
        let scale:CGFloat = screenWidth/imgWidth
        
        let rect:CGRect = CGRect(x: 0, y: 0, width: imgWidth*scale, height: imgHeight*scale)
        
        imageView.frame = rect
        
        imageView.center = CGPoint(x: screenWidth/2, y: screenHeight/2)
        
        self.view.addSubview(imageView)
    }
}

