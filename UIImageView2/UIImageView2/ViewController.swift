//
//  ViewController.swift
//  UIImageView2
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var image1 : UIImage!
    var image2 : UIImage!
    
    var flg = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        image1 = UIImage(named: "angelina-jolie-brad-pitt@2x.jpg")
        image2 = UIImage(named: "lena.jpg")
    }

    
    @IBAction func tapped(_ sender: AnyObject) {
        if flg{
            imageView.image = image1
            flg = false
        }else{
            imageView.image = image2
            flg = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

