//
//  ViewController.swift
//  UIImage_animation
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(red: 0.44, green: 0.41, blue: 0.36, alpha: 1.0)
    }
    
    @IBAction func start(_ sender: Any) {
        let image1 = UIImage(named: "angelina-jolie-brad-pitt@2x.jpg")!
        let image2 = UIImage(named: "lena.jpg")!
        
        var imageListArray : Array<UIImage> = []
        
        imageListArray.append(image1)
        imageListArray.append(image2)
        
        
        let screenWidth = self.view.bounds.width
        let screenHeight = self.view.bounds.height
        
        let imageWidth = image1.size.width
        let imageHeight = image1.size.height
        
        let imageView:UIImageView = UIImageView(image:image1)
        
        let rect = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        
        imageView.frame = rect
        
        imageView.center = CGPoint(x: screenWidth/2, y: screenHeight/2)
        
        self.view.addSubview(imageView)
        
        imageView.animationImages = imageListArray
        
        imageView.animationDuration = 3
        imageView.animationRepeatCount = 10
        
        imageView.startAnimating()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

