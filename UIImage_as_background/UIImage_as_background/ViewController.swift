//
//  ViewController.swift
//  UIImage_as_background
//
//  Created by huutrinh on 2019/07/02.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = UIImage(named: "angelina-jolie-brad-pitt@2x.jpg"){
            view.backgroundColor = UIColor(patternImage: image)
        }else{
            print("no images")
        }
    }


}

