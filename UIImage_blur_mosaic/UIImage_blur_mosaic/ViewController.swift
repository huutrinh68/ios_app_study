//
//  ViewController.swift
//  UIImage_blur_mosaic
//
//  Created by huutrinh on 2019/07/02.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        self.view.backgroundColor = UIColor(red:0.99,green:0.81,blue:0.56,alpha:1.0)
////        let img1 = UIImage(named: "handbag.png")
//        let img1 = UIImage(named: "angelina-jolie-brad-pitt@2x.jpg")
//        let imgView = UIImageView(image: img1)
//
//        let iWidth = img1!.size.width
//        let iHeight = img1!.size.height
//
//        let scale:CGFloat = 0.1
//        let sWidth = iWidth * scale
//        let sHeight = iHeight * scale
//
//        // Begin --->
//        UIGraphicsBeginImageContext(CGSize(width: sWidth, height: sHeight))
//        img1?.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: sWidth, height: sHeight))
//        let img2 = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//
//        UIGraphicsBeginImageContext(CGSize(width: iWidth, height: iHeight));
//        img2?.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0),
//                              width: iWidth, height: iHeight))
//        let img3 = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//        imgView.image = img3
//        let screenWidth:CGFloat = view.frame.size.width
//        let screenHeight:CGFloat = view.frame.size.height
//        imgView.center = CGPoint(x: screenWidth/2, y: screenHeight/2)
//
//        self.view.addSubview(imgView)
        
        
        self.view.backgroundColor = UIColor(red:0.99,green:0.81,blue:0.56,alpha:1.0)
        let img1 = UIImage(named: "angelina-jolie-brad-pitt@2x.jpg")!
        let imgView = UIImageView(image: img1)
        imgView.layer.shouldRasterize = true
        imgView.layer.rasterizationScale = 0.5
        
        let screenWidth:CGFloat = view.frame.size.width
        let screenHeight:CGFloat = view.frame.size.height
        
        imgView.center = CGPoint(x: screenWidth/2, y: screenHeight/2)
        
        self.view.addSubview(imgView)
    }


}

