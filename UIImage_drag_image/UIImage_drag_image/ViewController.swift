//
//  ViewController.swift
//  UIImage_drag_image
//
//  Created by huutrinh on 2019/07/02.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var labelX: UILabel!
    @IBOutlet weak var labelY: UILabel!
    
    let imageBag = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let screenWidth:CGFloat = view.frame.size.width
        let screenHeight:CGFloat = view.frame.size.height
        
        imageBag.image = UIImage(named: "handbag.png")
        imageBag.frame = CGRect(x: 0, y: 0, width: 128, height: 128)
        imageBag.center = CGPoint(x:screenWidth/2, y: screenHeight/2)
        
        imageBag.isUserInteractionEnabled = true
        
        self.view.addSubview(imageBag)
        
        labelX.text = "x: ".appendingFormat("%.2f", screenWidth/2)
        labelY.text = "y: ".appendingFormat("%.2f", screenHeight/2)
        
        self.view.backgroundColor = UIColor(red: 0.85, green: 1.0, blue: 0.95, alpha: 1.0)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesBegan")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchEvent = touches.first!
        
        let preDx = touchEvent.previousLocation(in: self.view).x
        let preDy = touchEvent.previousLocation(in: self.view).y
        
        let newDx = touchEvent.location(in: self.view).x
        let newDy = touchEvent.location(in: self.view).y
        
        let dx = newDx - preDx
        let dy = newDy - preDy
        print("x:\(dx)")
        print("y:\(dy)")
        
        var viewFrame: CGRect = imageBag.frame
        viewFrame.origin.x += dx
        viewFrame.origin.y += dy
        
        imageBag.frame = viewFrame
        
        labelX.text = "x: ".appendingFormat("%.2f", newDx)
        labelY.text = "y: ".appendingFormat("%.2f", newDy)
        
    }


}

