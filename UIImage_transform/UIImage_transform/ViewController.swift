//
//  ViewController.swift
//  UIImage_transform
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var imageView:UIImageView!
    var scale:CGFloat = 1.0
    var width:CGFloat = 0
    var height:CGFloat = 0
    var screenWidth:CGFloat = 0
    var screenHeight:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        screenWidth = self.view.bounds.width
        screenHeight = self.view.bounds.height
        
        let image = UIImage(named: "lena.jpg")!
        width = image.size.width
        height = image.size.height
        
        imageView = UIImageView(image: image)
        
        scale = screenWidth/width
        
        let rect:CGRect = CGRect(x: 0, y: 0, width: width*scale, height: height*scale)
        
        imageView.frame = rect
        
        imageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
        
        self.view.addSubview(imageView)
        
        
        let buttonPlus = UIButton()
        let buttonMinus = UIButton()
        
        buttonPlus.frame = CGRect(x: screenWidth/4-30, y: screenHeight-100, width: screenWidth/4+20, height: 35)
        buttonMinus.frame = CGRect(x: screenWidth/2+10, y: screenHeight-100, width: screenWidth/4+20, height: 35)
        
        buttonPlus.setTitle("+", for: UIControl.State.normal)
        buttonMinus.setTitle("-", for: UIControl.State.normal)
        
        buttonPlus.setTitleColor(UIColor.black, for: .normal)
        buttonMinus.setTitleColor(UIColor.black, for: .normal)
        
        buttonPlus.backgroundColor = UIColor.white
        buttonMinus.backgroundColor = UIColor.white
        
        buttonPlus.addTarget(self, action: #selector(ViewController.plus(sender:)), for: .touchUpInside)
        buttonMinus.addTarget(self, action: #selector(ViewController.minus(sender:)), for: .touchUpInside)
        self.view.addSubview(buttonPlus)
        self.view.addSubview(buttonMinus)
        
        self.view.backgroundColor = UIColor(red: 1.0, green: 0.8, blue: 0.5, alpha: 1.0)
    }
    
    @IBAction func plus(sender: AnyObject){
        if(width*scale < screenHeight*2){
            scale += 0.2
        }
        
        let rect:CGRect = CGRect(x: 0, y: 0, width: width*scale, height: height*scale)
        imageView.frame = rect
        imageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
        self.view.addSubview(imageView)
    }
    
    @IBAction func minus(sender: AnyObject){
        if(width*scale > screenHeight/4){
            scale -= 0.2
        }
        
        let rect:CGRect = CGRect(x: 0, y: 0, width: width*scale, height: height*scale)
        imageView.frame = rect
        imageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
        self.view.addSubview(imageView)
    }

}

