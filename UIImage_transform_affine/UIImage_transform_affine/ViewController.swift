//
//  ViewController.swift
//  UIImage_transform_affine
//
//  Created by huutrinh on 2019/07/01.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var transScale = CGAffineTransform()
    var transTrans = CGAffineTransform()
    var transRotate = CGAffineTransform()
    var transMiller = CGAffineTransform()
    
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        screenWidth = view.frame.size.width
        screenHeight = view.frame.size.height
        
        initImageView()
        
    }

    private func initImageView(){
        let image:UIImage = UIImage(named: "lena.jpg")!
        
        imageView.image = image
        
        imageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
        
        self.view.addSubview(imageView)
        
    }
    
    @IBAction func button(_ sender: Any) {
        if(count==0){
            transScale = CGAffineTransform(scaleX: 2, y: 2)
            imageView.transform = transScale
            label.text = "Scale" + String(count)
        }
        
        else if(count==1){
            transMiller = CGAffineTransform(scaleX: -1.5, y: 1.5)
            imageView.transform = transMiller
            label.text = "Miller" + String(count)
        }
        
        else if(count==2){
            let angle = 35*CGFloat.pi / 180
            transRotate = CGAffineTransform(rotationAngle: CGFloat(angle))
            imageView.transform = transRotate
            label.text = "Rotate" + String(count)
        }
        
        else if(count==3){
            transTrans = CGAffineTransform(translationX: 50, y: -30)
            imageView.transform = transTrans
            label.text = "Trans" + String(count)
        }
        
        else if(count==4){
            let trans1 = CGAffineTransform(scaleX: -1.5, y: 2.0)
            let trans2 = CGAffineTransform(translationX: -50, y: -30)
            let trans3 = trans1.concatenating(trans2)
            
            imageView.transform = trans3
            label.text = "Combine" + String(count)
        }
        
        else{
            imageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
            transScale = CGAffineTransform(scaleX: 1, y: 1)
            imageView.transform = transTrans
            label.text = "Original" + String(count)
            count = -1
        }
        count += 1
    }
}

