//
//  ViewController.swift
//  UILabel
//
//  Created by huutrinh on 2019/06/28.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let label = UILabel()
        
        // get screen size
        let screenWidth:CGFloat = view.frame.size.width
        let screenHeight:CGFloat = view.frame.size.height
        let margin:CGFloat = 1
        // setting absolute location
        label.frame = CGRect(x:margin, y:200, width:screenWidth-(margin*2), height:100)
        
        // setting text
        label.text = "Hello\nWorld"
        label.numberOfLines = 0
        
        // label.textColor = UIColor.blue
        let rgba = UIColor(red: 0.1, green: 0.7, blue: 1.0, alpha: 0.5)
        label.textColor = rgba

        // setting font
        label.font = UIFont.systemFont(ofSize: 30)
        label.textAlignment = NSTextAlignment.center
        
        // setting shadown
        label.shadowColor = UIColor.cyan
        label.shadowOffset = CGSize(width: 2, height: 2)
        
        // setting background
        label.backgroundColor = UIColor.lightGray
        
        // setting label to center
        // label.center = CGPoint(x: screenWidth/2, y:screenHeight/2)
        
        // setting relative location
        // label.bounds = CGRect(x:0, y:0, width: 100, height:100)
        
        // let labelPositionX:CGFloat = label.frame.origin.x
        // let labelPositionY:CGFloat = label.frame.origin.y
        // print("labelPositionX=\(labelPositionX)")
        // print("labelPositionY=\(labelPositionY)")
        
        // setting border
        label.layer.borderColor = UIColor.blue.cgColor
        label.layer.borderWidth = 2
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 10
        
        self.view.addSubview(label)
        
    }

}

