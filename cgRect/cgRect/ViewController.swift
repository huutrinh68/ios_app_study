//
//  ViewController.swift
//  cgRect
//
//  Created by huutrinh on 2019/06/28.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let testLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setLabel()
    }
    
    func setLabel(){
        let screenWidth:CGFloat = view.frame.size.width
        let screenHeight:CGFloat = view.frame.size.height
        
        testLabel.frame = CGRect(x:0, y:screenWidth-80, width:screenWidth, height:80)
        
        testLabel.text = "Hello World!"
        
        testLabel.textColor = UIColor.black
        
        testLabel.font = UIFont.boldSystemFont(ofSize: 30)
        
        testLabel.backgroundColor = UIColor(red: 0.7, green: 1.0, blue: 0.9, alpha: 1.0)
        
        testLabel.textAlignment = NSTextAlignment.center
        
        self.view.addSubview(testLabel)
    }


}

