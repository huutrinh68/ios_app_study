//
//  ViewController.swift
//  keyboard
//
//  Created by huutrinh on 2019/07/03.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        textField.delegate = self
    }

//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        label.text = textField.text
//        textField.resignFirstResponder()
//
//        return true
//    }
    
    
    @IBAction func button(_ sender: UIButton) {
        label.text = textField.text
        
        textField.endEditing(true)
    }
    
    @IBAction func getText(_ sender: UITextField) {
        label.text = sender.text
    }
    
}

