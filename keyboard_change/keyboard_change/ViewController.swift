//
//  ViewController.swift
//  keyboard_change
//
//  Created by huutrinh on 2019/07/04.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    @IBOutlet var name:UITextField!
    @IBOutlet var age:UITextField!
    @IBOutlet var phone:UITextField!
    @IBOutlet var email:UITextField!
    
    @IBOutlet var label:UILabel!
    @IBOutlet var button:UIButton!
    
    @IBOutlet var scrollView: UIScrollView!
    
    var screenHeight:CGFloat!
    var screenWidth:CGFloat!
    
    var data:[String] = ["Name: ", "Age: ", "Phone: ", "E-mail: "]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.delegate = self
        age.delegate = self
        phone.delegate = self
        email.delegate = self
        scrollView.delegate = self
        
        let screenSize: CGRect = UIScreen.main.bounds
        screenHeight = screenSize.width
        screenWidth = screenSize.height
        
        scrollView.frame.size = CGSize(width: screenWidth, height: screenHeight)
        scrollView.addSubview(name)
        scrollView.addSubview(age)
        scrollView.addSubview(phone)
        scrollView.addSubview(email)
        scrollView.addSubview(button)
        
        scrollView.contentSize = CGSize(width: screenWidth, height: screenHeight*1.5)
        scrollView.bounces = false
        
        name.keyboardType = UIKeyboardType.asciiCapable
        name.placeholder = "Name"
        
        age.keyboardType = UIKeyboardType.numberPad
        age.placeholder = "Age"
        
        phone.keyboardType = UIKeyboardType.numbersAndPunctuation
        phone.placeholder = "Phone"
        
        email.keyboardType = UIKeyboardType.asciiCapable
        email.placeholder = "Email"
        
        name.addTarget(self, action: #selector(self.nameFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        age.addTarget(self, action: #selector(self.ageFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        phone.addTarget(self, action: #selector(self.phoneFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        email.addTarget(self, action: #selector(self.emailFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        self.view.addSubview(scrollView)
    }
    
    @objc func nameFieldDidChange(textField: UITextField) {
        data[0] = "Name: " + name.text!
    }
    
    @objc func ageFieldDidChange(textField: UITextField) {
        data[1] = "Age: " + age.text!
    }
    
    @objc func phoneFieldDidChange(textField: UITextField) {
        data[2] = "Phone: " + phone.text!
    }
    @objc func emailFieldDidChange(textField: UITextField) {
        data[3] = "E-mail: " + email.text!
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        let str = data[0] + "\n" + data[1] + "\n" + data[2] + "\n" + data[3]
        label.text = str
        
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        let str = data[0] + "\n" + data[1] + "\n" + data[2] + "\n" + data[3]
        label.text = str
        
        view.endEditing(true)
        
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)) , name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: self.view.window)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        let info = notification.userInfo!
        
        let keyboardFrame = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let bottomButtonField = button.frame.origin.y + label.frame.height
        let topKeyboard = screenHeight - keyboardFrame.size.height
        let distance = bottomButtonField - topKeyboard
        
        if distance >= 0 {
            scrollView.contentOffset.y = distance + 20.0
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentOffset.y = 0
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
