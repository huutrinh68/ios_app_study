//
//  ViewController.swift
//  scrollView_big_image
//
//  Created by huutrinh on 2019/07/04.
//  Copyright © 2019 huutrinh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var scrollView = UIScrollView()
    

    var screenHeight:CGFloat!

    var screenWidth:CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenSize: CGRect = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        scrollView.frame.size =
            CGSize(width: screenWidth-10, height: screenWidth-10)
        scrollView.center = self.view.center
        
        let img:UIImage = UIImage(named:"angelina-jolie-brad-pitt@2x.jpg")!
        
        let imgW = img.size.width
        let imgH = img.size.height
        
        let imageView = UIImageView(image: img)
        
        scrollView.addSubview(imageView)
        
        scrollView.contentSize = CGSize(width: imgW, height: imgH)
        
        scrollView.bounces = false
        
        self.view.addSubview(scrollView)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


